
#include <iostream>
#include <time.h>

using namespace std;

int main()
{
    //set array size
    const int N = 7;
  
    //initialize array N * N
    int array[N][N];
        for (int i = 0; i < N; i++) 
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }

    //print array elements
        cout << "Array:\n\n";
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
                        {
                cout << array[i][j] << " ";
            }
        }
        cout << "\n";
    }
    
    cout << "\n";
   

    //get day V1
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int day = buf.tm_mday;
    cout << "Current day is : " << day << "\n";

    //get day V2
    time_t Time = time(NULL);
    struct tm* aTime = localtime(&Time);
    int day2 = aTime->tm_mday;

    cout << "Current day2 is : " << day2 << "\n";


    //calculate array string
    int Modulus = day % N;
    cout << "Modulus of Day/N : " << day << "/" << N << " = " << Modulus << "\n";


    //calculate sum
    int sum = 0;
    for (int j = 0; j < N; j++)
    {
        sum += array[Modulus][j];
    }

    //print sum
    cout << "Sum of Elements: ";
    cout << sum << "\n";

    
       
}

        


   

